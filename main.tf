provider "azurerm" {
  
  version = "1.38.0"
# Azure Service principal Info
subscription_id = "b225741d-6d57-42ca-957b-88df8e1897f1"
# client_id or app_id
client_id = "50c3c830-057d-4fc4-9fcd-2f75f93b180c"
client_secret = "ZflAOIcsHDbZdALN6G66hcaxokS3.2zKBZ"
tenant_id = "2b32dc2a-e594-4e75-b488-f83157986b73"
  
}

# resource "azurerm_resource_group" "testtf" {
#   name     = "testtf"
#   location = "eastus"
# }

# data "azurerm_virtual_network" "test" {
#   name                = "production"
#   resource_group_name = "testtf"
# }

# output "virtual_network_id" {
#   value = "${data.azurerm_virtual_network.test.id}"
# }

#Resources
resource "azurerm_resource_group" "test" {
  name     = "testResourceGroup1"
  location = "West US"

  tags = {
    environment = "Production"
  }
}
#Virtual Machine
resource "azurerm_virtual_network" "test" {
  name                = "test-network"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.test.location
  resource_group_name = azurerm_resource_group.test.name
}

resource "azurerm_subnet" "test" {
  name                 = "internal"
  resource_group_name  = azurerm_resource_group.test.name
  virtual_network_name = azurerm_virtual_network.test.name
  address_prefixes     = ["10.0.2.0/24"]
}

resource "azurerm_network_interface" "test" {
  name                = "test-nic"
  location            = azurerm_resource_group.test.location
  resource_group_name = azurerm_resource_group.test.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.test.id
    private_ip_address_allocation = "Dynamic"
  }
}

resource "azurerm_windows_virtual_machine" "test" {
  name                = "test-machine"
  resource_group_name = azurerm_resource_group.test.name
  location            = azurerm_resource_group.test.location
  size                = "Standard_F2"
  admin_username      = "adminuser"
  admin_password      = "P@$$w0rd1234!"
  network_interface_ids = [
    azurerm_network_interface.test.id,
  ]

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = "2016-Datacenter"
    version   = "latest"
  }
}



